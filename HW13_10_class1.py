import requests
import json


class Photo:
    def __init__(self,id,title,url,thumbnailUrl):
        self.id=id
        self.title=title
        self.url=url
        self.thumbnailUrl=thumbnailUrl
        #buildind class
    def __str__(self):
        return f'Photo: [id] {self.id} [title] {self.title} [url] {self.url} [thumbnailUrl] {self.thumbnailUrl}'
        #print class definition when call the class
    def __repr__(self):
        return f'Photo: [id] {self.id} [title] {self.title} [url] {self.url} [thumbnailUrl] {self.thumbnailUrl}'
        #explain how build the class
    def __eq__(self, other):
        #fix what equal in class.if id equal resturn true
        if self.id==other.id:
            return True
        else:
            return False
    def __gt__(self, other):
        #fix what bigger in class.if id bigger resturn true
        if self.id>other.id:
            return True
        else:
            return False
    def __lt__(self, other):
        #fix what smaller in class.if id smaller resturn true
        if self.id<other.id:
            return True
        else:
            return False


class QuickPhoto:
    def __init__(self, d):
        #build the class from dict
        for k, v in d.items():
            self.__dict__[k] = v

    def __str__(self):
        #print the definition from dict
        result = ""
        for k, v in self.__dict__.items():
            result += k
            result += " : "
            result += str(v)
            result += ' , '
        return result[:-3]


class Album:
    def __init__(self,QuickPhoto,pic1,pic2,pic3):
        #build the Album class
        self.QuickPhoto=QuickPhoto
        self.pic1=pic1
        self.pic2=pic2
        self.pic3=pic3
    def __str__(self):
        #print the Album class
        return f'Album: [QickPhoto] {self.QuickPhoto} [pic1] {self.pic1} [pic2] {self.pic2} [pic3] {self.pic3}'

#create 2 object from Photo class
moshe=Photo(3,'moshe','https://via.placeholder.com/600/92c952','https://via.placeholder.com/150/92c952')
yosef=Photo(2,'yosef','https://via.placeholder.com/600/92c952','https://via.placeholder.com/150/92c952')

if moshe==yosef:
    print(moshe.title+'=='+yosef.title)
else:
    print(moshe.title+' different '+yosef.title)
#Verif if id's sames
if moshe>yosef:
    print(moshe.title+'>'+yosef.title)
else:
    print(moshe.title+' < '+yosef.title)
#Verif if id bigger

if moshe<yosef:
    print(moshe.title+'<'+yosef.title)
else:
    print(moshe.title+' > '+yosef.title)
#Verif if id smaller

resp1=requests.get("https://jsonplaceholder.typicode.com/photos/1")
d=json.loads(resp1.content)
#get request from rest server and transform in jason form

jeremy=Photo(d['id'],d['title'],d['url'],d['thumbnailUrl'])
print(jeremy)
# Create and print jeremy in type Photo class

momo=QuickPhoto(d)
print(momo)
# Create and print momo in type QuickPhoto class


from PIL import Image
img = Image.open('C:/Users/jeremy/Pictures/viago.png')
img.show()
#open png file

for n in range(1, 11):
    #load and print the 10 person from rest server
    resp2=requests.get("https://jsonplaceholder.typicode.com/photos/"+str(n))
    exemp="photo"+str(n)
    d = json.loads(resp2.content)
    exemp=QuickPhoto(d)
    print(exemp)

testAlbum=Album(momo,'pic1','pic2','pic3')
print(testAlbum)
# Create and print testAlbum in type Album class

count=1
idNumber = input("input id you want: ")
while True:
    resp3=requests.get("https://jsonplaceholder.typicode.com/photos/"+str(count))
    d = json.loads(resp3.content)
    if resp3.status_code!=200:
        print("dont find your id")
        break
        #if you go in end of list server because you read all server id, status code not send 200 that is ok, and  out from while

    if int(d['id'])==int(idNumber):
        findId = QuickPhoto(d)
        print(findId)
        break
        #if find the id in rest server,create findId in QuickPhoto class
    else:
        count=count+1






